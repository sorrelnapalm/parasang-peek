using ConsoleLib.Console;
using XRL.World;
using XRL.World.Parts;
using ParasangPeek.Configs;
using ParasangPeek.Util;

namespace XRL.UI
{

// much thanks to the CavesOfQuickMenu mod! I learned a lot by reading its code at
// https://github.com/dan-kest/Qud-CavesOfQuickMenu
public class ParasangPeek_MapUI
{

private const int X1 = PREVIEW.ratio * 2;
private const int Y1 = 1;
private const int X2 = 79 - X1;
private const int Y2 = 24 - Y1;

private static ScreenBuffer Buffer;
private static ScreenBuffer OldBuffer;

private static XRL.World.Parts.ParasangPeek_Map map => XRL.The.Player.GetPart<XRL.World.Parts.ParasangPeek_Map>();

// resolved x and y
private static int center_rx => center_wx * 3 + center_x;
private static int center_ry => center_wy * 3 + center_y;
private static int center_x;
private static int center_y;
private static int center_z;
private static int center_wx;
private static int center_wy;

private static void Draw()
{
    CenterAtPlayer();
    XRL.Messages.MessageQueue.AddPlayerMessage("Drawing Map UI:");
    XRL.Messages.MessageQueue.AddPlayerMessage("rx: " + center_rx.ToString());
    XRL.Messages.MessageQueue.AddPlayerMessage("ry: " + center_ry.ToString());
    XRL.Messages.MessageQueue.AddPlayerMessage("z: " + center_z.ToString());
    if (Buffer != null) {
        ParasangPeek_ConsoleUtils.SuppressScreenBufferImposters(true, X1, Y1, X2, Y2);
        Buffer.BeveledBox(X1, Y1, X2, Y2, ColorUtility.Bright(TextColor.Black), ColorUtility.Bright(TextColor.Cyan));
        Buffer.Fill(X1 + 1, Y1 + 1, X2 - 1, Y2 - 1, 32, ColorUtility.MakeColor(TextColor.Grey, TextColor.Black));
        DrawZonePreview(X1 + 10, Y1 + 4, center_rx, center_ry, center_z);
        Popup._TextConsole.DrawBuffer(Buffer);
    }
}

private static void Erase() {
    if (Buffer != null && OldBuffer != null) {
        Popup._TextConsole.DrawBuffer(OldBuffer);
        ParasangPeek_ConsoleUtils.SuppressScreenBufferImposters(false, X1, Y1, X2, Y2);
    }
}

public static int Show() {
    GameManager.Instance.PushGameView(SCREEN.LEGACY_MAP_UI);
    TextConsole.LoadScrapBuffers();
    Buffer = TextConsole.ScrapBuffer;
    OldBuffer = TextConsole.ScrapBuffer2;
    Draw();
    while (true) {
        Keys input = Keyboard.getvk(false);
        string cmd = LegacyKeyMapping.GetCommandFromKey(input);

        if (input == Keys.Escape || cmd == "CmdCancel") {
            Erase();
            GameManager.Instance.PopGameView();
            return PARASANG_PEEK_SCREEN_CODE.NONE;
        } else {
            XRL.Messages.MessageQueue.AddPlayerMessage(cmd);
        }
    }
}

private static void DrawZonePreview(int screenX, int screenY, int resolvedX, int resolvedY, int Z) {
    string id = XRL.World.Zone.XYToID("JoppaWorld", resolvedX, resolvedY, Z);

    ParasangPeek_MapNode node = map.GetNode(id);
    if (node != null) {
        node.Update();
        ParasangPeek_ZonePreview preview = node.Preview;
        Cell[,] cells = preview.Cells;
        if (cells.IsNullOrEmpty()) return;
        if (screenX < 0 || screenX >= 80) return; 
        if (screenY < 0 || screenY >= 25) return; 

        for (int y = screenY; y < screenY + preview.Height; ++y) {
            for (int x = screenX; x < screenX + preview.Width; ++x) {
                // draw somehow
                int preview_x = x - screenX;
                int preview_y = y - screenY;

                if (x < 0 || x >= 80) continue;
                if (y < 0 || y >= 25) continue;

                Cell cell = cells[preview_x, preview_y];
                if (cell == null) continue;

                UnityEngine.Debug.Log("preview size is " + preview.Width.ToString() + "x" + preview.Height.ToString());
                UnityEngine.Debug.Log("screen topleft coords: (" + screenX.ToString() + ", " + screenY.ToString() + ")");
                UnityEngine.Debug.Log("preview coords: (" + preview_x.ToString() + ", " + preview_y.ToString() + ")");
                UnityEngine.Debug.Log("tile destination coords: (" + x.ToString() + ", " + y.ToString() + ")");
                if (!string.IsNullOrEmpty(cell.PaintTile)) {
                    UnityEngine.Debug.Log("paint tile " + cell.PaintTile);
                }
                if (!string.IsNullOrEmpty(cell.PaintRenderString)) {
                    UnityEngine.Debug.Log("paint render str " + cell.PaintRenderString);
                }
                if (!string.IsNullOrEmpty(cell.PaintColorString)) {
                    UnityEngine.Debug.Log("paint color str " + cell.PaintColorString);
                }
                if (!string.IsNullOrEmpty(cell.PaintTileColor)) {
                    UnityEngine.Debug.Log("paint tile color " + cell.PaintTileColor);
                }
                if (!string.IsNullOrEmpty(cell.PaintDetailColor)) {
                    UnityEngine.Debug.Log("paint detail color " + cell.PaintDetailColor);
                }

                //Buffer.Goto(x, y);
                //Buffer.Write(cell.PaintTile, cell.PaintRenderString, cell.PaintColorString, cell.PaintTileColor, cell.PaintDetailColor, true, false, false);
                if (!cell.PaintTile.IsNullOrEmpty() && Options.UseTiles) {
                    Buffer[x, y].Tile = cell.PaintTile;
                    UnityEngine.Debug.Log("setting tile for ConsoleChar (" + x.ToString() + "," + y.ToString() + ") to " + cell.PaintTile);
                }
                if (!cell.PaintRenderString.IsNullOrEmpty() && !Options.UseTiles) {
                    Buffer[x, y].Char = cell.PaintRenderString[0];
                    UnityEngine.Debug.Log("setting char for ConsoleChar (" + x.ToString() + "," + y.ToString() + ") to " + cell.PaintTile);
                }
            }
        }

        // debug
        //Cell cell = cells[0, 0];
        //if (cell != null) {
        //    UnityEngine.Debug.Log("preview size is " + preview.Width.ToString() + "x" + preview.Height.ToString());
        //    UnityEngine.Debug.Log("screen topleft coords: (" + screenX.ToString() + ", " + screenY.ToString() + ")");
        //    if (!string.IsNullOrEmpty(cell.PaintTile)) UnityEngine.Debug.Log("paint tile " + cell.PaintTile);
        //    if (!string.IsNullOrEmpty(cell.PaintRenderString)) UnityEngine.Debug.Log("paint render str " + cell.PaintRenderString);
        //    if (!string.IsNullOrEmpty(cell.PaintColorString)) UnityEngine.Debug.Log("paint color str " + cell.PaintColorString);
        //    if (!string.IsNullOrEmpty(cell.PaintTileColor)) UnityEngine.Debug.Log("paint tile color " + cell.PaintTileColor);
        //    if (!string.IsNullOrEmpty(cell.PaintDetailColor)) UnityEngine.Debug.Log("paint detail color " + cell.PaintDetailColor);
        //    Buffer.Goto(screenX, screenY);
        //    Buffer.Write(cell.PaintTile, cell.PaintRenderString, cell.PaintColorString, cell.PaintTileColor, cell.PaintDetailColor);
        //}

        // status bar?
        Buffer.Goto(X1 + 2, Y2 - 1);
        Buffer.Write("test test test long string status bar");
    }
}

private static void CenterAtPlayer() {
    Zone z = XRL.The.ActiveZone;

    // if on world map, get the parasang that the player is at and center map
    // on the center zone in that parasang
    if (!z.ZoneID.Contains(".")) {
        center_wx = XRL.The.PlayerCell.X;
        center_wx = XRL.The.PlayerCell.Y;
        center_x = 1;
        center_y = 1;
        center_z = 10;
    } else {
        center_wx = z.wX;
        center_wy = z.wY;
        center_x = z.X;
        center_y = z.Y;
        center_z = z.Z;
    }
}

} // ParasangPeek_MapUI

} // namespace