using ConsoleLib.Console;
using System;
using XRL;
using XRL.Core;
using XRL.Rules;
using ParasangPeek.Configs;

// thank you to https://wiki.cavesofqud.com/wiki/Modding:Random_Functions for
// a very helpful example
namespace ParasangPeek.Util
{
[HasGameBasedStaticCache]
public static class ParasangPeek_Random
{
    private static string seedString = ParasangPeek.Configs.MODINFO.MOD_NAME + ":Random";
    private static Random _rand;
    public static Random Rand {
        get {
            if (_rand == null) {
                if (XRLCore.Core?.Game == null) {
                    throw new Exception();
                } else if (XRLCore.Core.Game.IntGameState.ContainsKey(seedString)) {
                    int seed = XRLCore.Core.Game.GetIntGameState(seedString);
                    _rand = new Random(seed);
                } else {
                        _rand = Stat.GetSeededRandomGenerator(ParasangPeek.Configs.MODINFO.MOD_NAME);
                }
                XRLCore.Core.Game.SetIntGameState(seedString, _rand.Next());
            }
            return _rand;
        }
    }

    [GameBasedCacheInit]
    public static void ResetRandom() {
        _rand = null;
    }

    // min, max are inclusive
    public static int Next(int min, int max) {
        return Rand.Next(min, max + 1);
    }
} // ParasangPeek_Random

public static class ParasangPeek_ConsoleUtils
{

    public static void SuppressScreenBufferImposters(bool isSuppress=true, int x1 = 0, int y1 = 0, int x2 = 79, int y2 = 24) {
        for (int y = y1; y <= y2; ++y) {
            for (int x = x1; x <= x2; ++x) {
                ScreenBuffer.ImposterSuppression[x, y] = isSuppress;
            }
        }        
    }

} // ParasangPeek_ConsoleUtils


public static class ParasangPeek_OverlayUtils
{

}

} // namespace