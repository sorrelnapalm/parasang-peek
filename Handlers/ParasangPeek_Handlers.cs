using XRL;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;

namespace ParasangPeek.Handlers
{
    [PlayerMutator]
    public class ParasangPeek_NewGameHandler : IPlayerMutator
    {
        public void mutate(GameObject player)
        {
            player.AddPart<ParasangPeek_CommandListener>();
            player.AddPart<ParasangPeek_Map>();
        }
    }

    [HasCallAfterGameLoadedAttribute]
    public class ParasangPeek_LoadGameHandler
    {
        [CallAfterGameLoadedAttribute]
        public static void LoadGameCallback()
        {
            GameObject player = XRLCore.Core?.Game?.Player?.Body;
            if (player != null) {
                player.RequirePart<ParasangPeek_CommandListener>();
                player.RequirePart<ParasangPeek_Map>();
            }
        }
    }
}