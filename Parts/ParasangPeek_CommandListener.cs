using System;

namespace XRL.World.Parts
{
    [Serializable]
    public class ParasangPeek_CommandListener : IPart
    {
        public override void Register(GameObject Object)
        {
            Object.RegisterPartEvent(this, ParasangPeek.Configs.COMMANDS.OPEN);
            base.Register(Object);
        }

        public override bool FireEvent(Event E)
        {
            if (E.ID == ParasangPeek.Configs.COMMANDS.OPEN) {
                XRL.Messages.MessageQueue.AddPlayerMessage("Parasang Peek Open Map Command");
                XRL.Messages.MessageQueue.AddPlayerMessage(XRL.The.ActiveZone.ZoneID);

                XRL.UI.ParasangPeek_MapUI.Show();
            }
            return base.FireEvent(E);
        }
    }
}