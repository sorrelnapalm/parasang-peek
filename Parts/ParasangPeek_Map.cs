using System;
using System.Collections.Generic;
using SerializeField = UnityEngine.SerializeField;
using XRL.World;

namespace XRL.World.Parts
{
[Serializable]
public class ParasangPeek_Map : IPart
{
    public Dictionary<string, ParasangPeek_MapNode> _map;
    private string _last; // keep track of the previous zone activated

    /* Initializes empty map structure e.g. on new game
     * 
     * 
     */
    public ParasangPeek_Map() {
        _map = new Dictionary<string, ParasangPeek_MapNode>();
        _last = null;
    }

    public ParasangPeek_MapNode GetNode(string id) {
        if (_map.ContainsKey(id)) {
            return _map[id];
        } else {
            return null;
        }
    }
    public override bool HandleEvent(ZoneActivatedEvent E) {
        Zone z = E.Zone;
        string id = z.ZoneID;
        UnityEngine.Debug.Log("Parasang Peek handling ZoneActivatedEvent");
        UnityEngine.Debug.Log("for zone " + id);

        // don't add the world map to the dict if it was activated
        if (!id.Contains(".")) {
            UnityEngine.Debug.Log("not adding world map to dict");
        } else {
            // check if we have old info on this zone in our map
            if (_map.ContainsKey(id)) {
                ParasangPeek_MapNode n = _map[id];
                /* we don't immediately need to update b/c player is
                 * presumably still running around in the zone,
                 * so just invalidate for now. info will actually be
                 * updated when a different zone is activated
                 */
                n.Invalidate();
                UnityEngine.Debug.Log("Invalidated cached data for zone " + id);
            } else {
                // add zone to our dict; this is the first time the player has
                // activated the zone
                ParasangPeek_MapNode n = new ParasangPeek_MapNode(id);
                // upon creating the new MapNode we cache info for the zone (in the constructor)
                // before the player has done anything, so we mark the node as invalid
                // so the cached info gets updated again after the player leaves
                n.Invalidate();
                _map.Add(id, n);
                UnityEngine.Debug.Log("Adding new zone " + id + " to cache");
            }

            UnityEngine.Debug.Log("size of map: " + _map.Count.ToString());
        }

        /* update previous zone activated.
         * happens also if moving from a zone to the world map.
         * if moving from the world map into a zone _last will become the world
         * map in which case we don't update anything in our dict.
         * when moving from a zone to the world map we set _last to the world
         * map after updating information for that zone.
         */
        if (_last != null && _last.Contains(".")) {
            if (_map.ContainsKey(_last)) {
                UnityEngine.Debug.Log("last zone " + _last + " in map, updating...");
                ParasangPeek_MapNode l = _map[_last];
                l.Update();
                UnityEngine.Debug.Log("new _last is " + id);
            } else {
                UnityEngine.Debug.LogError("Error! last zone activated not in map. This should never happen.");
                UnityEngine.Debug.LogError("zone snapshot may not be most recent.");
            }

            UnityEngine.Debug.Log("new _last is " + id);
            _last = id;
        } else {
            // either _last is null, or _last is the world map,
            // so we don't do anything besides update _last.
            // TODO: track exploration of the world map?
            _last = id;
        }

        return true;
    }
    public override bool WantEvent(int ID, int cascade) {
        return base.WantEvent(ID, cascade)
        || ID == ZoneActivatedEvent.ID;
    }

} // public class ParasangPeek_Map
} // namespace XRL.World