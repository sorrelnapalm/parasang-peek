using System.Collections.Generic;
using XRL.World;

namespace XRL.World.Parts
{
public enum ParasangPeek_CellTypes : int
{
        Solid,
        Edge,
        Liquid,
        Staircase,
        Empty,
        Other,
        Count,
}

public class ParasangPeek_ZonePreview
{
    public Zone Zone;
    private const int _ratio = ParasangPeek.Configs.PREVIEW.ratio; // ratio of preview width/height to zone width/height respectively
    private int _width;
    public int Width {
        get {
            if (Zone != null) {
                _width = Zone.Width / _ratio;
            } else {
                _width = 0;
            }
            return _width;
        }
    }
    public int _height;
    public int Height {
        get {
            if (Zone != null) {
                _height = Zone.Height / _ratio;
            } else {
                _height = 0;
            }
            return _height;
        }
    }

    private Cell[,] _cells;
    public Cell[,] Cells {
        get {
            if (Zone != null && _cells == null) {
                GeneratePreview();
            }
            return _cells;
        }
    }

    public void GeneratePreview() {
        if (Zone == null) {
            DestroyPreview();
            return;
        }

        if (_cells == null) {
            _cells = new Cell[Width,Height];
        }
        UnityEngine.Debug.Log("generating preview for zone " + Zone.ZoneID);
        for (int y = 0; y < Height; ++y) {
            for (int x = 0; x < Width; ++x) {
                _cells[x, y] = GetChunkAverage(x, y);
            }
        }
    }

    public void DestroyPreview() {
        _cells = null;
    }

    public int GetCellType(Cell cell) {
        if (cell.IsSolid()) {
            return (int)ParasangPeek_CellTypes.Solid;
        } else if (cell.IsEdge()) {
            return (int)ParasangPeek_CellTypes.Edge;
        } else if (cell.HasWadingDepthLiquid()) {
            return (int)ParasangPeek_CellTypes.Liquid;
        } else if (!cell.IsPassable()) {
            return (int)ParasangPeek_CellTypes.Staircase;
        } else if (cell.IsEmpty()) {
            return (int)ParasangPeek_CellTypes.Empty;
        } else {
            return (int)ParasangPeek_CellTypes.Other;
        }
    }
    public int GetPriority(Cell cell) {
        if (cell == null) return 0;

        return GetPriority(GetCellType(cell));
    }

    public int GetPriority(int type) {
        if (type == (int)ParasangPeek_CellTypes.Solid) {
            return 25;
        } else if (type == (int)ParasangPeek_CellTypes.Edge) {
            return 10;
        } else if (type == (int)ParasangPeek_CellTypes.Liquid) {
            return 20;
        } else if (type == (int)ParasangPeek_CellTypes.Staircase) {
            return 1000;
        }

        return 1;
    }

    /* Averages a 5x5 area of a zone into a single cell.
     * Prioritizes objects like liquids, trees, statues, other obstacles, staircases.
     * Doesn't care about corpses and such objects that can be stood on.
     * Ground tiles receive lowest priority.
     */
    public Cell GetChunkAverage(int chunk_x, int chunk_y) {
        UnityEngine.Debug.Log("averaging chunk " + chunk_x.ToString() + "x" + chunk_y.ToString());
        //UnityEngine.Debug.Log("zone getcells: length " + cells.Count.ToString());

        int zone_height = Zone.Height;
        int zone_width = Zone.Width;
        //UnityEngine.Debug.Log("zone is " + zone_width.ToString() + "x" + zone_height.ToString());

        // find which cell in the chunk should be used to represent the chunk
        int count = (int)ParasangPeek_CellTypes.Count;
        int[] tally = new int[count];
        for (int i = 0; i < count; ++i) {
            tally[i] = 0;
        }

        for (int x = chunk_x * 5; x < (chunk_x+1) * 5; ++x) {
            for (int y = chunk_y * 5; y < (chunk_y+1) * 5; ++y) {
                int type = GetCellType(Zone.GetCell(x, y));
                int prio = GetPriority(Zone.GetCell(x, y));

                if (0 <= type && type < count) {
                    tally[type] += prio;
                }
            }
        }

        // debug
        for (int i = 0; i < count; ++i) {
            UnityEngine.Debug.Log("tally[" + i.ToString() + "] = " + tally[i].ToString());
        }

        // find which type is the most represented
        List<int> most_rep_types = new List<int>();
        int max = 0;
        for (int i = 0; i < count; ++i) {
            if (tally[i] > max) {
                max = tally[i];
            }
        }

        for (int i = 0; i < count; ++i) {
            if (tally[i] == max) {
                most_rep_types.Add(i);
            }
        }

        // debug
            UnityEngine.Debug.Log("most rep types:");
        for (int i = 0; i < most_rep_types.Count; ++i) {
            UnityEngine.Debug.Log("type " + most_rep_types[i].ToString());
        }

        // tiebreak between cell types with the same score by choosing
        // the one with highest priority
        int highest_prio_type = -1;
        int highest_prio = 0;
        for (int i = 0; i < most_rep_types.Count; ++i) {
            int type = most_rep_types[i];
            int prio = GetPriority(type);
            if (highest_prio < prio) {
                highest_prio_type = type;
                highest_prio = prio;
            }
        }
        
        UnityEngine.Debug.Log("type w/ highest prio: " + highest_prio_type.ToString());
        UnityEngine.Debug.Log("prio is: " + highest_prio.ToString());


        // return a random cell of that type in the 5x5 chunk
        List<Cell> cells_of_type = new List<Cell>();
        for (int x = chunk_x * 5; x < (chunk_x+1) * 5; ++x) {
            for (int y = chunk_y * 5; y < (chunk_y+1) * 5; ++y) {
                Cell cell = Zone.GetCell(x, y);
                int type = GetCellType(cell);
                if (type == highest_prio_type) {
                    Cell newCell = new Cell(this.Zone);
                    newCell.X = chunk_x;
                    newCell.Y = chunk_y;

                    newCell.PaintRenderString = (string.IsNullOrEmpty(cell.PaintRenderString)) ? " " : cell.PaintRenderString;
                    newCell.PaintTile = (string.IsNullOrEmpty(cell.PaintTile)) ? string.Empty : cell.PaintTile;
                    newCell.PaintColorString = (string.IsNullOrEmpty(cell.PaintColorString)) ? string.Empty : cell.PaintColorString;
                    newCell.PaintDetailColor = (string.IsNullOrEmpty(cell.PaintDetailColor)) ? "\0" : cell.PaintDetailColor;
                    newCell.PaintTileColor = (string.IsNullOrEmpty(cell.PaintTileColor)) ? "\0" : cell.PaintTileColor;

                    cells_of_type.Add(newCell);
                }
            }
        }

        //UnityEngine.Debug.Log("all " + cells_of_type.Count.ToString() + "cells of that type in chunk:");
        for (int i = 0; i < cells_of_type.Count; ++i) {
            Cell c = cells_of_type[i];
            //UnityEngine.Debug.Log("x: " + c.X.ToString() + " y: " + c.Y.ToString());
            //UnityEngine.Debug.Log("PaintRenderString: " + c.PaintRenderString);
            //UnityEngine.Debug.Log("PaintTileColor: " + c.PaintTileColor);
            //UnityEngine.Debug.Log("PaintColorString: " + c.PaintColorString);
            //UnityEngine.Debug.Log("PaintTile: " + c.PaintTile);
            //UnityEngine.Debug.Log("PaintDetailColor: " + c.PaintDetailColor);
        }

        int rand = ParasangPeek.Util.ParasangPeek_Random.Rand.Next(0, cells_of_type.Count - 1);
        //UnityEngine.Debug.Log("rand is: " + rand.ToString());
        return cells_of_type[rand];
    }

    public ParasangPeek_ZonePreview(Zone zone) {
        this.Zone = zone;
        this._cells = null;
    }

    public ParasangPeek_ZonePreview() {
        this.Zone = null;
        this._cells = null;
    }

    public override string ToString() {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        for (int y = 0; y < 5; ++y) {
            for (int x = 0; x < 16; ++x) {
                builder.AppendLine(Cells[x,y].ToString());
            }
        }

        return builder.ToString();
    }

} // ParasangPeek_ZonePreview

} // namespaceXRL.World.Parts