using System;
using System.Collections.Generic;
using SerializeField = UnityEngine.SerializeField;
using XRL.World;

namespace XRL.World.Parts
{
[Serializable]
public class ParasangPeek_MapNode
{

    // cached zone info
    public int X;
    public int Y;
    public int Z;
    public int wX;
    public int wY;

    public string Direction {
        get {
            if (X == 0 && Y == 0) {
                return "NW";
            } else if (X == 0 && Y == 1) {
                return "W";
            } else if (X == 0 && Y == 2) {
                return "SW";
            } else if (X == 1 && Y == 0) {
                return "N";
            } else if (X == 1 && Y == 1) {
                return "C";
            } else if (X == 1 && Y == 2) {
                return "S";
            } else if (X == 2 && Y == 0) {
                return "NE";
            } else if (X == 2 && Y == 1) {
                return "E";
            } else if (X == 2 && Y == 2) {
                return "SE";
            }
            
            return "ERR";
        }
    }
    public int numCellsExplored;
    public int numCellsReachable;

    public float ExploredRatio => (float)numCellsExplored / (float)numCellsReachable;

    [NonSerialized]
    private ParasangPeek_ZonePreview _preview;

    public ParasangPeek_ZonePreview Preview {
        get {
            if (_preview == null && Zone != null) {
                _preview = new ParasangPeek_ZonePreview(Zone);
                _preview.GeneratePreview();
            }

            return _preview;
        }
    }

    public int resolvedX;
    public int resolvedY;

    public bool Valid;

    public long LastPlayerPresence;

    public long LastCacheUpdate;

    public string ZoneID;

    [NonSerialized]
    private Zone _zone;

    public Zone Zone {
        get {
            if (_zone == null) {
                _zone = XRL.The.ZoneManager.GetZone(ZoneID);
            }

            return _zone;
        }
    }

    public void Invalidate() {
        Valid = false;

        if (Preview != null) Preview.DestroyPreview();
    }

    // update cached zone info
    public void Update() {
        if (Valid) {
            return;
        }

        numCellsReachable = NumReachableCells();
        numCellsExplored = NumExploredCells();
        LastPlayerPresence = Zone.LastPlayerPresence;
        LastCacheUpdate = XRL.The.CurrentTurn;

        if (Preview != null) Preview.DestroyPreview();
        Preview.GeneratePreview();

        Valid = true;
    }

    public int NumReachableCells() {
        int num = 0;
        for (int x = 0; x < Zone.Width; ++x) {
            for (int y = 0; y < Zone.Height; ++y) {
                if (Zone.IsReachable(x, y)) ++num;
            }
        }

        return num;
    }

    public int NumExploredCells() {
        int num = 0;
        for (int x = 0; x < Zone.Width; ++x) {
            for (int y = 0; y < Zone.Height; ++y) {
                if (Zone.ExploredMap[x + y * Zone.Width]) ++num;
            }
        }

        return num;
    }

    public ParasangPeek_MapNode(string ID) {
        Zone zone = XRL.The.ZoneManager.GetZone(ID);
        X = zone.X;
        Y = zone.Y;
        Z = zone.Z;
        wX = zone.wX;
        wY = zone.wY;
        resolvedX = zone.resolvedX;
        resolvedY = zone.resolvedY;
        ZoneID = zone.ZoneID;
        _preview = null;
        // update dynamic zone info such as last player presence, explored ratio
        Invalidate(); // so Update() does something
        Update();
    }

    public override string ToString() {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.AppendLine("zone id: " + ZoneID + " (" + Valid.ToString() + ")");
        builder.AppendLine("entry last updated: " + LastCacheUpdate.ToString());
        builder.AppendLine("last player presence: " + LastPlayerPresence.ToString());
        builder.AppendLine("updated after lpp: " + (LastCacheUpdate > LastPlayerPresence).ToString());
        builder.AppendLine("explored ratio: " + numCellsExplored.ToString() + " / " + numCellsReachable.ToString() + " = " + ExploredRatio.ToString());

        return builder.ToString();
    }

} // public class ParasangPeek_MapNode
} // namespace XRL.World