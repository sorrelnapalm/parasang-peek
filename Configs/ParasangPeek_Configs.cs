
namespace ParasangPeek.Configs
{

    public class MODINFO
    {
        public const string MOD_NAME = "ParasangPeek";
    }

    public class COMMANDS
    {
        public const string OPEN = "ParasangPeek_OpenMapCmd";
    }

    public class PREVIEW
    {
        public const int ratio = 5; // each dimension is scaled down by this factor
    }

    public class PARASANG_PEEK_SCREEN_CODE
    {
        public const int NONE = -1234;
    }

    public class SCREEN
    {
        public const string LEGACY_MAP_UI = MODINFO.MOD_NAME + ":" + "LegacyMapUI";
    }
}