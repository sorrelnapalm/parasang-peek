using System.Collections.Generic;

using XRL.Wish;
using XRL.World.Parts;

[HasWishCommand]
public static class ParasangPeek_DumpMapWish
{
    public const string WISH_NAME = "parasangpeekdumpmap";
    [WishCommand(Command = WISH_NAME)]
    public static bool HandleWish()
    {
        // dump map to Player.log
        ParasangPeek_Map pp_map = XRL.The.Player.GetPart<ParasangPeek_Map>();
        Dictionary<string, ParasangPeek_MapNode> map = pp_map._map;
        UnityEngine.Debug.Log("-----------------------------");
        UnityEngine.Debug.Log("ParasangPeek Dump Map Wish");
        UnityEngine.Debug.Log("-----------------------------");
        UnityEngine.Debug.Log("\tNumber of elements: " + map.Count.ToString());

        int count = 0;

        foreach(var e in map) {
            ++count;
            string id = e.Key;
            ParasangPeek_MapNode n = e.Value;

            UnityEngine.Debug.Log("zone " + count.ToString());
            UnityEngine.Debug.Log(n.ToString());
        }

        return true;
    }
}