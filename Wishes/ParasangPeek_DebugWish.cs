using XRL.Wish;
using XRL.World;

[HasWishCommand]
public static class ParasangPeek_DebugWish
{
    public const string WISH_NAME_DEBUG = "parasangpeekdebug";

    [WishCommand(Command = WISH_NAME_DEBUG)]
    public static bool HandleWish()
    {
        Zone activeZone = ZoneManager.instance.ActiveZone;

        XRL.Messages.MessageQueue.AddPlayerMessage("ParasangPeek Debug Wish");
        XRL.Messages.MessageQueue.AddPlayerMessage("ZoneID: " + activeZone._ZoneID);
        XRL.Messages.MessageQueue.AddPlayerMessage("ZoneWorld: " + activeZone.ZoneWorld);

        return true;
    }

}
